const mongodb = require("mongodb");

const getDb = require("../util/database").getDb;

class Query {
  constructor(name, category, message){
    this.name = name;
    this.category = category;
    this.message = message;
  }
  save(){
    const db = getDb();
    let dbOp;
    dbOp = db.collection("queries").insertOne(this)
    return dbOp
      .then(result=>{
      })
      .catch(err=>{
        console.log(err);
      
    })
  }

  static fetchAll(){
    const db = getDb();
    return db.collection("queries")
    .find()
    .toArray()
    .then(queries=>{
        return queries;
    })
    .catch(err=>{
      console.log(err);
      }
    )
  }

  static findById(queryID){
    const db = getDb();
    return db.collection("queries")
    .find({_id: new mongodb.ObjectID(queryID)})
    .next()
    .then(query=>{
      return query;
    })
    .catch(err=>{
      console.log(err);
      }
    )
  }

  static deleteById(queryId){
    const db = getDb();
    return db.collection("queries")
    .deleteOne({_id: new mongodb.ObjectID(queryId)})
    .then(result=>{
    })
    .catch(err=>{
      console.log(err);
      })
  }
};
module.exports = Query;