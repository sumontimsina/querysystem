const mongodb = require("mongodb");

const getDb = require("../util/database").getDb;

class Response {
  constructor(status, remarks, queryId, name, category , message){
    this.status = status;
    this.remarks = remarks;
    this.queryId = new mongodb.ObjectID(queryId);
    this.name = name;
    this.category = category;
    this.message = message;
  }
  save(){
    const db = getDb();
    let dbOp;
    dbOp = db.collection("responses").insertOne(this)
    return dbOp
      .then(result=>{
      })
      .catch(err=>{
        console.log(err);
      
    })
  }

  static fetchAll(){
    const db = getDb();
    return db.collection("responses")
    .find()
    .toArray()
    .then(queries=>{
        return queries;
    })
    .catch(err=>{
      console.log(err);
      }
    )
  }

//    findById(queryID){
//     const db = getDb();
//     return db.collection("queries")
//     .find({_id: new mongodb.ObjectID(queryID)})
//     .next()
//     .then(query=>{
//       return staticquery;
//     })
//     .catch(err=>{
//       console.log(err);
//       }
//     )
//   }

  static deleteById(resId, queryId){
    const db = getDb();
    return db.collection("responses")
    .deleteOne({_id: new mongodb.ObjectID(resId)})
    .then(result=>{
        db.collection("queries")
        .deleteOne({_id: new mongodb.ObjectID(queryId)})
        .then(result=>{
        })
        })
    .catch(err=>{
      console.log(err);
      })
  }
};
module.exports = Response;