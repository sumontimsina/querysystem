const express = require("express");
const clientController = require("../controllers/client");


const router = express.Router();

router.get("/client", clientController.getClient);

router.get("/client/query", clientController.getQuery);

router.get("/client/responses", clientController.getResponses);

router.post("/client/responses", clientController.postQuery);

router.post("/client/delete-response", clientController.postDeleteResponse);
 
// router.get("/products", shopController.getProducts);

// router.get("/products/:productId", shopController.getProductDetails)

// router.get("/cart", shopController.getCart);

// router.post("/cart", shopController.postCart);

// router.post("/cart-delete-item", shopController.postCartDeleteProduct);

// router.post("/create-order", shopController.postOrders);

// router.get("/orders", shopController.getOrders);

// router.get("/ckeckout", shopController.getCheckout);

module.exports = router;
