const express = require("express");
const adminController = require("../controllers/admin");


const router = express.Router();

router.get("/", adminController.getIndex);

router.get("/admin", adminController.getQueries);

router.get("/admin/response/:queryId", adminController.getResponse);




// router.post("/add-product", adminController.postAddProduct);

// router.get("/edit-product/:productId", adminController.getEditProduct);

router.post("/admin", adminController.postResponse);

router.post("/admin/delete-query", adminController.postDeleteQuery)


module.exports = router;