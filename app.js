const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const errorController = require("./controllers/404");

const adminRoutes = require("./routes/admin-route");
const clientRoute = require("./routes/client-route");
const mongoConnect = require("./util/database").mongoConnect;


const app = express();

app.set("view engine" ,"ejs");
app.set("views", "views");



app.use(bodyParser.urlencoded({extended: false}));

app.use(express.static(path.join(__dirname, "public")));



app.use( adminRoutes);
app.use( clientRoute);

app.use(errorController.get404);

mongoConnect(()=>{
    app.listen(3001);
  })
