const Query = require("../models/query");
const Response = require("../models/response");


exports.getIndex = (req, res, next) => {
    res.status(404).render("index", {pageTitle: "Home", path: "/"});
};

exports.getAdmin = (req, res, next) => {
    res.render("admin/admin",
        {
            pageTitle: "Admin",
            path: "/admin"
        }
    )
};

exports.getQueries = (req, res, next) =>{
    Query.fetchAll().then(queries=>{
        res.render("admin/query-list", 
        {       queries: queries,
                pageTitle: "All Queries", 
                path: "/admin"
        });
    }).catch(err=>{
        console.log(err);
    });
};

exports.getResponse = (req, res, next) =>{
    const queryId = req.params.queryId;
    Query.findById(queryId)
        .then(query=>{
            res.render('admin/response', {
                query: query,
                pageTitle: "Admin Response",
                path: '/admin'
                });
            
        })
        .catch(err=>{
            console.log(err);
        })
};

exports.postResponse = (req, res ,  next)=>{
    const status = req.body.status;
    const remarks = req.body.remarks;
    const queryId = req.body.queryId;
    const name = req.body.name;
    const category = req.body.category;
    const message = req.body.message;
    const response = new Response(status, remarks, queryId, name, category , message);
    response.save()
    .then(result=>{
        res.redirect("/admin");
        })
        .catch(err=>{
            console.log(err)
        });
};

exports.postDeleteQuery = (req, res, next)=>{
    const queryId = req.body.queryId;
    Query.deleteById(queryId)
        .then(result=>{
            res.redirect("/admin");
        })
        .catch(err=>console.log(err));
    
};