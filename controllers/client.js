const Query = require("../models/query");
const Response = require("../models/response");


exports.getClient = (req, res, next) => {
    res.render("client/client",
        {
            pageTitle: "Client",
            path: "/client"
        }
    )
};

exports.getQuery = (req, res, next) => {
    res.render("client/query",
        {
            pageTitle: "Client",
            path: "/client"
        }
    )
};
exports.postQuery = (req, res ,  next)=>{
    const name = req.body.name;
    const category = req.body.category;
    const message = req.body.query;
    const query = new Query(name, category, message);
    query.save()
    .then(result=>{
        res.redirect("/client/responses");
        })
        .catch(err=>{
            console.log(err)
        });
};

exports.getResponses = (req, res, next) => {
    Response.fetchAll().then(responses=>{
        res.render("client/response", 
        {       responses: responses,
                pageTitle: "All Responses", 
                path: "/client"
        });
    }).catch(err=>{
        console.log(err);
    });
};

exports.postDeleteResponse = (req, res, next)=>{
    const queryId = req.body.queryId;
    const resId = req.body.resId;
    Response.deleteById(resId,queryId)
        .then(result=>{
            res.redirect("/client/responses");

            // Query.deleteById(queryId)
            // .then(result =>{
            // })
        })
        .catch(err=>console.log(err));
    
};
